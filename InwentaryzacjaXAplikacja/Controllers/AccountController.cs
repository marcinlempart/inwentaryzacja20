﻿using System;
using System.Threading.Tasks;
using InwentaryzacjaXAplikacja.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InwentaryzacjaXAplikacja.Controllers
{   
    public class AccountController : Controller
    {
        private readonly UserManager<UserModel> _userManager;
        private readonly SignInManager<UserModel> _signInManager;

        public AccountController(UserManager<UserModel> userManager, SignInManager<UserModel> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Login userLoginrData)
        {
            if (!ModelState.IsValid) return RedirectToAction("LoginError", "Error");

            var result = _signInManager.PasswordSignInAsync(userLoginrData.UserName, userLoginrData.Password, false,
                false);
            if (result.Result.Succeeded) return RedirectToAction("Index", "Home");

            return RedirectToAction("LoginError", "Error");
        }

        [HttpGet]
        [Route("Register156234123")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [Route("Register156234123")]
        public async Task<IActionResult> RegisterUser(Register userRegisterData)
        {
            if (!ModelState.IsValid) return View(userRegisterData);

            await _userManager.CreateAsync(new UserModel
            {
                Email = userRegisterData.Email,
                UserName = userRegisterData.UserName
            }, userRegisterData.Password);


            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "home");
        }
    }
}