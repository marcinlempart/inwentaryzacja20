﻿using Microsoft.AspNetCore.Mvc;

namespace InwentaryzacjaXAplikacja.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult WarehouseDeleteError()
        {
            return View();
        }

        public IActionResult ProductIsAvailableError()
        {
            return View();
        }

        public IActionResult LoginError()
        {
            return View();
        }
    }
}   