﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using InwentaryzacjaXAplikacja.Models;
using InwentaryzacjaXAplikacja.Models.DTOS;
using OfficeOpenXml;


namespace InwentaryzacjaXAplikacja.Controllers
{
    public class ExcelDataController : Controller
    {
        private IHostingEnvironment _hostingEnv;
        private DbAppContext _dbContext;

        public ExcelDataController(IHostingEnvironment hostingEnv, DbAppContext context)
        {
            _hostingEnv = hostingEnv;
            _dbContext = context;
        }

        public IActionResult UploadExcel()
        {
            return View();
        }


        public async Task<IActionResult> ImportExcel(IFormFile file)
        {
            var list = new List<ProductDTO>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (var row = 2; row <= rowcount; row++)
                        list.Add(new ProductDTO
                        {
                            Code = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            Name = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            Quantity = Convert.ToInt32(worksheet.Cells[row, 3].Value.ToString().Trim()),
                            CategoryId = Convert.ToInt32(worksheet.Cells[row, 4].Value.ToString().Trim())
                        });

                    foreach (var body in list)
                    {
                        var product = new Product
                        {
                            Name = body.Name,
                            Code = body.Code,
                            IsAvailable = true,
                            Quantity = (int)body.Quantity,
                            CategoryId = body.CategoryId
                        };

                        _dbContext.Products.Add(product);
                        _dbContext.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }
    }
}