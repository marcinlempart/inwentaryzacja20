﻿using InwentaryzacjaXAplikacja.Models;
using InwentaryzacjaXAplikacja.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InwentaryzacjaXAplikacja.Controllers
{
    public class ProductController : Controller
    {
        private readonly IWarehouseService _warehouseService;
        private readonly DbAppContext _context;

        public ProductController(IWarehouseService warehouseService, DbAppContext context)
        {
            _warehouseService = warehouseService;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Product()
        {
            return View();
        }

        public IActionResult List()
        {
            var productList = _context.Products.ToList();
            return View(productList);
        }

        [HttpPost]
        [ActionName("GetProductsByCategoryId")]
        public JsonResult GetProductsByCategoryId(string categoryId)
        {
            int catId;
            var productLists = new List<Product>();
            var result = new List<Product>();

            var inventoryList = _context.ControlInventory.ToList();


            if (!string.IsNullOrEmpty(categoryId))
            {
                catId = Convert.ToInt32(categoryId);
                var invetoryList = _context.ControlInventory.ToList();

                productLists = _context.Products
                    .Where(s => s.CategoryId.Equals(catId))
                    .ToList();

                foreach (var item in productLists)
                {
                    var productAfterInventory = inventoryList.Where(x => x.ProductId == item.Id).ToList();
                    if (productAfterInventory.Count > 0)
                    {
                        Console.WriteLine("NIe ma");
                    }
                    else
                    {
                        var np = new Product
                        {
                            Id = item.Id,
                            Name = item.Name,
                            CategoryId = catId
                        };
                        result.Add(np);
                    }
                }


                var invent = _context.ControlInventory.ToList();
            }

            return Json(result);
        }
    }
}