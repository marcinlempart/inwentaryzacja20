﻿using InwentaryzacjaXAplikacja.Models;
using InwentaryzacjaXAplikacja.Models.DTOS;
using InwentaryzacjaXAplikacja.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace InwentaryzacjaXAplikacja.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly IWarehouseService _warehouseService;
        private readonly DbAppContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public WarehouseController(IWarehouseService warehouseService, DbAppContext context,
            IHostingEnvironment hostingEnvironment)
        {
            _warehouseService = warehouseService;
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var product = _context.Products.Where(x => x.Id == Id).FirstOrDefault();
            var _categories = _context.Category.ToList();
            var lista = _context.ControlInventory.ToList();
            _categories.Add(new Category() { Id = 0, Name = "--Wybierz kategorię--" });

            ViewData["CategoryData"] = new SelectList(_categories.OrderBy(s => s.Id), "Id", "Name");

            var host = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/";
            ViewData["UrlGive"] = host;


            return View(product);
        }

        [HttpPost]
        public IActionResult Edit(Product body)
        {
            var product = _context.Products.Where(x => x.Id == body.Id).FirstOrDefault();
            if (product != null)
            {
                product.Code = body.Code;
                product.Name = body.Name;
                product.Quantity = body.Quantity;
                product.CategoryId = body.CategoryId;
            }

            _context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpGet]
        public IActionResult ExportToExcel()
        {
            var stream = _warehouseService.ReturnStreamToExcel();
            var excelName2 = $"ProductList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName2);
        }

        

        [HttpGet]
        public IActionResult Add()
        {
            var _categories = _context.Category.ToList();
            var lista = _context.ControlInventory.ToList();

            var _products = new List<Product>();
            _categories.Add(new Category() { Id = 0, Name = "--Wybierz kategorię--" });
            _products.Add(new Product() { Id = 0, Name = "--Wybierz produkt--" });

            ViewData["CategoryData"] = new SelectList(_categories.OrderBy(s => s.Id), "Id", "Name");
            ViewData["ProductData"] = new SelectList(_products.OrderBy(s => s.Id), "Id", "Name");

            var host = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/";
            ViewData["UrlGive"] = host;

            return View();
        }

        [HttpPost]
        public IActionResult Add(Order body)
        {
            if (!ModelState.IsValid) return View(body);

            _warehouseService.SaveProductToDatabase(body);

            return RedirectToAction("List");
        }

        

        [HttpGet]
        public IActionResult List()
        {
            var result = _warehouseService.ListResultControlInventory().ToList();

            return View(result);
        }

        

        [HttpGet]
        public IActionResult Details(int id)
        {
            var product = _warehouseService.Get(id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var product = _context.Products.Where(i => id == i.Id).FirstOrDefault();

            if (product.Quantity > 0)
            {
                return RedirectToAction("ProductIsAvailableError", "Error");
            }
            else
            {
                _warehouseService.Delete(id);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public IActionResult RemoveCategory(Order body)
        {
            var category = _context.Category.Where(i => i.Id == body.CategoryId).FirstOrDefault();
            var products = _context.Products.Where(i => i.CategoryId == body.CategoryId).ToList();

            if (products.Count > 0)
            {
                ViewData["MessageDelete"] = string.Format("Remove", DateTime.Now.ToString());
                return RedirectToAction("WarehouseDeleteError", "Error");
            }
            else
            {
                _context.Category.Remove(category);
                _context.SaveChanges();
                return RedirectToAction("List");
            }
        }

        public IActionResult AddCategory()
        {
            var _categories = _context.Category.ToList();
            var lista = _context.ControlInventory.ToList();

            var _products = _context.Products.ToList();
            _categories.Add(new Category() { Id = 0, Name = "--Wybierz kategorię--" });
            _products.Add(new Product() { Id = 0, Name = "--Wybierz produkt--" });

            ViewData["CategoryData"] = new SelectList(_categories.OrderBy(s => s.Id), "Id", "Name");
            ViewData["ProductData"] = new SelectList(_products.OrderBy(s => s.Id), "Id", "Name");

            var host = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/";
            ViewData["UrlGive"] = host;


            return View();
        }

        [HttpPost]
        public IActionResult AddCategory(CategoryDto body)
        {
            var ct2 = new Category
            {
                Name = body.Name
            };
            _context.Category.Add(ct2);
            _context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpGet]
        public IActionResult AddSecondProduct()
        {
            var _categories = _context.Category.ToList();
            var _products = new List<Product>();

            _categories.Add(new Category() { Id = 0, Name = "--Wybierz kategorię--" });
            _products.Add(new Product() { Id = 0, Name = "--Wybierz produkt--" });

            ViewData["CategoryData"] = new SelectList(_categories.OrderBy(s => s.Id), "Id", "Name");
            ViewData["ProductData"] = new SelectList(_products.OrderBy(s => s.Id), "Id", "Name");

            var host = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/";
            ViewData["UrlGive"] = host;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Order order)
        {
            _warehouseService.CreateProduct(order);

            return RedirectToAction("Index", "Home");
        }
    }
}