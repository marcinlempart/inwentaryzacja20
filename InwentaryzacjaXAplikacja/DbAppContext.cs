﻿using InwentaryzacjaXAplikacja.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity.Infrastructure;

namespace InwentaryzacjaXAplikacja
{
    public class DbAppContext : IdentityDbContext<UserModel>
    {
        public DbAppContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<ProductInventoryView> ProductInventoryViews { get; set; }
        public DbSet<ControlInventory> ControlInventory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }   
    }
}
