﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InwentaryzacjaXAplikacja.Migrations
{
    public partial class inventoryControl2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryControl");

            migrationBuilder.DropIndex(
                name: "IX_ControlInventory_ProductId",
                table: "ControlInventory");

            migrationBuilder.CreateIndex(
                name: "IX_ControlInventory_ProductId",
                table: "ControlInventory",
                column: "ProductId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ControlInventory_ProductId",
                table: "ControlInventory");

            migrationBuilder.CreateTable(
                name: "InventoryControl",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    QuatityControl = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryControl", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryControl_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ControlInventory_ProductId",
                table: "ControlInventory",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryControl_ProductId",
                table: "InventoryControl",
                column: "ProductId",
                unique: true);
        }
    }
}
