﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InwentaryzacjaXAplikacja.Models
{
    public class ControlInventory
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public int QuatityControl { get; set; }
        public virtual Product Product { get; set; }

    }
}
