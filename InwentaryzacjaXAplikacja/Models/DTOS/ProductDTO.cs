﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InwentaryzacjaXAplikacja.Models.DTOS
{
    public class ProductDTO
    {
        public string? Code { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public decimal? Price { get; set; }

        public bool? IsAvailable { get; set; }
        public int? Quantity { get; set; }
        public int? QuantityInventory { get; set; }

        public int CategoryId { get; set; }
    }
}
