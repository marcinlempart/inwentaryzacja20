﻿using System.ComponentModel.DataAnnotations;

namespace InwentaryzacjaXAplikacja.Models.DTOS
{
    public class ProductToExcelDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int? Quantity { get; set; }
        public int? QuantityControl { get; set; }
    }
}
