﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InwentaryzacjaXAplikacja.Models
{
    public class Order
    {
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public string Name { get; set; }
        public string Quantity { get; set; }
        public int QuantityBody { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }


        [Display(Name = "Category")]
        [ForeignKey("CategoryId")]
        public virtual int CategoryId { get; set; }

        [Display(Name = "Product")]
        [ForeignKey("ProductId")]
        public virtual long ProductId { get; set; }

        [Display(Name = "Category")]
        public virtual Category? CategoryInfo { get; set; }

        [Display(Name = "Product")]
        public virtual Product? Product { get; set; }

    }
}
