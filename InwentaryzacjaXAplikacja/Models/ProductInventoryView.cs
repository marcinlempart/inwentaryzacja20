﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace InwentaryzacjaXAplikacja.Models
{
    public class ProductInventoryView
    {
        public int Id { get; set; }

        public string Code { get; set; }
        public int QuantityControl { get; set; }

        [Required]
        public string Name { get; set; }

        [MaxLength(255)]
        public string? Description { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public bool IsAvailable { get; set; }
        public int Quantity { get; set; }

        public int CategoryId { get; set; }

        [JsonIgnore]
        public virtual Category Category { get; set; }
        public virtual ControlInventory ControlInventory { get; set; }
    }
}