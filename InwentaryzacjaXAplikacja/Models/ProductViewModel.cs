﻿using System.ComponentModel.DataAnnotations;

namespace InwentaryzacjaXAplikacja.Models
{
    public class ProductViewModel
    {   
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string InventoryQuantity { get; set; }
        public int InventoryDifference { get; set; }
        public decimal Price { get; set; }
    }
}
