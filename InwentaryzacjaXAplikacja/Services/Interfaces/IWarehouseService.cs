﻿using System.Collections.Generic;
using System.IO;
using InwentaryzacjaXAplikacja.Models;

namespace InwentaryzacjaXAplikacja.Services.Interfaces
{
    public interface IWarehouseService
    {
        int Save(Product product);
        List<Product> GetAll();
        Product Get(int id);

        int Delete(int id);
        IEnumerable<ProductViewModel> ListResultControlInventory();
        void SaveProductToDatabase(Order body);
        MemoryStream ReturnStreamToExcel();
        void CreateProduct(Order order);
    }
}
