﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using InwentaryzacjaXAplikacja.Models;
using InwentaryzacjaXAplikacja.Models.DTOS;
using InwentaryzacjaXAplikacja.Services.Interfaces;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace InwentaryzacjaXAplikacja.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly DbAppContext _dbAppContext;

        public WarehouseService(DbAppContext dbAppContext)
        {
            _dbAppContext = dbAppContext;
        }

        public int Delete(int id)
        {
            var product = _dbAppContext.Products.Find(id);
            _dbAppContext.Remove(product);
            _dbAppContext.SaveChanges();
            return id;
        }
        public int Save(Product product)
        {
            _dbAppContext.Add(product);
            _dbAppContext.SaveChanges();
            return product.Id;
        }

        public List<Product> GetAll()
        {
            return _dbAppContext.Products.ToList();
        }

        public Product Get(int id)
        {
            var product = _dbAppContext.Products.Find(id);
            return product;
        }

        public IEnumerable<ProductViewModel> ListResultControlInventory()
        {
            var productList = _dbAppContext.Products.ToList();
            var inventory = _dbAppContext.ControlInventory.ToList();


            var result3 =
                from p in productList
                join value1 in inventory
                    on p.Id equals value1.ProductId
                    into eGroup
                from value1 in eGroup.DefaultIfEmpty()
                select new ProductViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Quantity = p.Quantity,
                    InventoryQuantity = value1 == null ? "brak inwentaryzacji" : value1.QuatityControl.ToString(),
                    Price = p.Price,
                    InventoryDifference = value1 == null ? 0 : value1.QuatityControl - p.Quantity
                };
            return result3;
        }

        public void SaveProductToDatabase(Order body)
        {
            var product = new Product
            {
                Name = body.Name,
                Code = body.Code,
                Description = "",
                Price = 0,
                CategoryId = body.CategoryId,
                IsAvailable= true,    
            };

            _dbAppContext.Products.Add(product);
            _dbAppContext.SaveChanges();
        }

        public MemoryStream ReturnStreamToExcel()
        {
            var productList = _dbAppContext.Products.Select(x => new ProductToExcelDTO()
            {
                Name = x.Name,
                Code = x.Code,
                Quantity = x.Quantity,
                QuantityControl = x.ControlInventory.QuatityControl
            }).ToList();
            var table =
                (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(productList), typeof(DataTable));
            var stream = new MemoryStream();
            using (var excelPack = new ExcelPackage(stream))
            {
                var ws = excelPack.Workbook.Worksheets.Add("ProductData");
                ws.Cells.LoadFromCollection(productList, true);
                excelPack.Save();
            }

            stream.Position = 0;
            return stream;
        }

        public void CreateProduct(Order order)
        {
            var ic = new ControlInventory
            {
                ProductId = (int)order.ProductId,
                QuatityControl = order.QuantityBody
            };
                _dbAppContext.ControlInventory.Add(ic);
                _dbAppContext.SaveChanges();
        }
    }
}
