#pragma checksum "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4776c549f1c9df63f6d865b68a55bb8c279f7c55"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Warehouse_List), @"mvc.1.0.view", @"/Views/Warehouse/List.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\_ViewImports.cshtml"
using InwentaryzacjaXAplikacja;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\_ViewImports.cshtml"
using InwentaryzacjaXAplikacja.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
using Microsoft.CodeAnalysis.CSharp.Syntax;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4776c549f1c9df63f6d865b68a55bb8c279f7c55", @"/Views/Warehouse/List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a234f239098e63ec58a27fb139e2a807100a9c4b", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Warehouse_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ProductViewModel>>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral("    \r\n<html>\r\n<h1>Lista produktów</h1>\r\n\r\n");
            WriteLiteral("<table class=\"table table-bordered mt-4\">\r\n    <thead>\r\n    <tr>\r\n");
            WriteLiteral(@"            <th scope=""col"" style=""text-align: center"">Nazwa produktu</th>
        <th scope=""col"" style=""text-align: center"">Stana magazynowy</th>
        <th scope=""col"" style=""text-align: center"">Stana zgodny z inwentaryzacją</th>
        <th scope=""col"" style=""text-align: center"">Różnica inwentaryzacyjna</th>
        <th scope=""col"" style=""text-align: center"" >Usuń produkt z listy</th>
        <th scope=""col"" style=""text-align: center"" >Edytuj produkt</th>
    </tr>
    </thead>
    <tbody>



");
#nullable restore
#line 43 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
     foreach (var product in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td style =\"text-align: center\">");
#nullable restore
#line 46 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                                       Write(product.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            <td style=\"text-align: center\">");
#nullable restore
#line 47 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                                      Write(product.Quantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            <td style=\"text-align: center\">");
#nullable restore
#line 48 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                                      Write(product.InventoryQuantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            \r\n");
#nullable restore
#line 50 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
             if (@product.InventoryQuantity.Contains("brak inwentaryzacji"))
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <td style=\"text-align: center; color:black\">brak inwentaryzacji</td>\r\n");
#nullable restore
#line 53 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
            }
            else{
                if(@product.InventoryDifference < 0)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <td style=\"text-align: center; color: red\">");
#nullable restore
#line 57 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                                                          Write(product.InventoryDifference);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n");
#nullable restore
#line 58 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <td style=\"text-align: center; color: greenyellow\" >");
#nullable restore
#line 61 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                                                                       Write(product.InventoryDifference);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n");
#nullable restore
#line 62 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                }
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("            <td style=\"text-align: center\">\r\n");
#nullable restore
#line 65 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                 using (Html.BeginForm("Delete", "Warehouse", FormMethod.Post))
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <input type=\"hidden\" name=\"Id\"");
            BeginWriteAttribute("value", " value=\"", 2379, "\"", 2398, 1);
#nullable restore
#line 67 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
WriteAttributeValue("", 2387, product.Id, 2387, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("/>\r\n                    <button type=\"submit\" class=\"btn btn-primary\" >Usuń</button>\r\n");
#nullable restore
#line 69 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </td>\r\n            <td style=\"text-align: center\">\r\n                <a");
            BeginWriteAttribute("href", " href=\"", 2586, "\"", 2649, 1);
#nullable restore
#line 72 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
WriteAttributeValue("", 2593, Url.Action("Edit", "Warehouse", new { Id = product.Id}), 2593, 56, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Edytuj</a>\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 75 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n    \r\n    \r\n    <a");
            BeginWriteAttribute("href", " href=\"", 2736, "\"", 2784, 1);
#nullable restore
#line 79 "C:\Users\matus\InwentaryzacjaXAplikacja_v10\InwentaryzacjaXAplikacja\Views\Warehouse\List.cshtml"
WriteAttributeValue("", 2743, Url.Action("ExportToExcel", "Warehouse"), 2743, 41, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Exportuj dane do excela</a>\r\n</table>\r\n</html>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ProductViewModel>> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
